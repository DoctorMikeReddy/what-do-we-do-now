#-Info-
﻿//I should probably use the same format for all the others here. > names a dialog if it's the first character, # begins a comment.//
//comments are inside two pairs of double-slashes//
//use a hash (#) to begin a number or text label//
//using special symbols in dialog://
// \< = < //
// \> = > //
// \$ = $ //
// \\ = \ //

#Start
<setattr [game start true]>
<setattr [game infilleds true]>
<setattr [game equilaterals true]>
<setattr [game scalenes true]>
Press ENTER to advance,<but> X to zoom or Z to skip dialogue.<but>...but why would you do that?<but>
 The Global Game Jam is Great.<but> You feel a strange desire to give them all your money.<but>
<noskip><clear>...but please refrain from using the X, and Z keys if you'd like to fully enjoy the experience. <but><clear>
Please note, this game is NOT finished.<but>Working alone, and having to sleep prevented me from getting everything done. <but>
So, this is really a place holder.<but> Please check back in a week or two, and the GGJ web site will have an updated version.<but> Sorry!<but>
<skip><dg "First Menu">

#Omega
<showname Omega>
What do you want?<but><br>
<if <getattr [game  infilleds] >>You have impaled the Infilleds!
<else>The Infilleds infest us!
<endif><but><br>
<if <getattr  [game scalenes] >>You have slaughtered the Scalenes!
<else>The Scalenes have not suffered enough!
<endif><but><br>
<if <getattr [game equilaterals] >>You have eliminated the Equilaterals!
<else>The Equilaterals exist still!
<endif>
<but><dg "Main Choice">

#Main Choice
<showname Omega>
What do we do now?
<option [
	["Impale the Infilleds!" "infilleds"  <getattr [game infilleds]>]
        ["Slaughter the Scalene Scum!" "scalenes"  <getattr [game scalenes]>]
	["Kill the Equilateral Traitors!" "equilaterals" <getattr [game equilaterals]>]
	["Praise be to al-Gebra'da!" "Bye"]
]>

#First Menu
Use Up and Down arrows to navigate lists, and ENTER to select.<but><br>
Do you understand?
<option [
	["Yes, I understand. Now can I play?" "Jump Start"]
        ["No, I have the attenti...ooh, shiny!" "Start" ]
	["Praise be to the great al-Gebra'da!" "Bye"]
]>

#infilleds
<setattr [game infilleds false]>
The Infilleds will be impaled!<but><dg "Main Choice">

#scalenes
<setattr [game scalenes false]>
The Scalene Scum will surely suffer.<but><dg "Main Choice">

#equilaterals
<setattr [game equilaterals false]>
The Equilaterals will easily be eliminated.<but><dg "Main Choice">
]>

#Bye
Come back again!<but><end>

#Text Flow
Hello, <wait 0.4>this is an example of dynamic text flow. <but>Please refrain from using the C, X, and Z keys if you'd like to fully enjoy everything this particular demonstration has to offer. <but><clear>As you can see, <wait 0.2>we're pausing the text after every comma<wait 0.2>.<wait 0.4>.<wait 0.4>. <wait 0.6>and during ellipses.<but><clear>
This is accomplished by using the {cb}\<wait #\>{cd} command. <but>You may have also noticed us pausing and awaiting a button input. <but>This is accomplished by using the {cb}\<but\>{cd} command. <but><clear>
<typespeed 0.02>We can also change the character drawing speed. <wait 0.1>We're drawing faster-<wait 0.4> exactly 0.02 seconds per character. <but><typespeed 0.15>Now we're drawing slowly-<wait 0.4> exactly 0.15 seconds per character. <but><clear>
<typespeed 0.05>We accomplish this by using the {cb}\<typespeed #\>{cd} command.<but><dg "Main Choice">

#Visual Text
<shake>This is shaking text.</shake> <but>Shaking text is accomplished by using the {cb}\<shake\>{cd} command.<but><br>
<sine>This is sine-wave text.</sine> <but>Sine-wave text can be accomplished by using the {cb}\<sine\>{cd} command.<but><br>
<grow>This is growing (actually, shrinking) text.</grow> <but>Growing text can be accomplished by using the {cb}\<grow\>{cd} command.<but><br>
<revolve>This is revolving text.</revolve> <but>Revolving text can be accomplished by using the {cb}\<revolve\>{cd} command.<but><br>
<color #F31D1D>This is colored text.<color -1> <but>Colored text can be accomplished by using the {cb}\<color HEX_COLOR\>{cd} command.<but><br>
<font Big>This text uses a different font.<but><font White><br>
You can change the font using the {cb}\<font FONT\>{cd} command.<but>
<clear>
<typespeed 0.08><shake>I- <wait 0.4>I- <wait 0.6><typespeed 0.05><font "Small White">I'm so scared<font White><typespeed 0.08><wait 0.2>.<wait 0.4>.<wait 0.4>. <wait 0.5>I have a <wait 0.09>f-<wait 0.12> feeling that a <wait 0.4>s-<wait 0.08> s-<wait 0.5><typespeed 0.05>spooky ghost is present.<but><clear>
<revolve><sine><grow><typespeed 0.1><font Big><color #FF5A4B>Oh my gosh!<color -1><font White></grow></sine></revolve></shake><but><dg "Main Choice">

#Sound
<stopchan 29><playchan Piano 29><sine>{gm2}</sine><but><dg "Main Choice">

#CJK
<font Handdrawn>The following is Japanese, but the concept extends to any unicode-covered language.<bc>
<font Wagahai><typespeed 0.02><typesound none>
吾輩わがはいは猫である。名前はまだ無い。<bc>
どこで生れたかとんと見当けんとうがつかぬ。何でも薄暗いじめじめした所でニャーニャー泣いていた事だけは記憶している。吾輩はここで始めて人間というものを見た。しかもあとで聞くとそれは書生という人間中で一番獰悪どうあくな種族であったそうだ。この書生というのは時々我々を捕つかまえて煮にて食うという話である。しかしその当時は何という考もなかったから別段恐しいとも思わなかった。<bc>
ただ彼の掌てのひらに載せられてスーと持ち上げられた時何だかフワフワした感じがあったばかりである。掌の上で少し落ちついて書生の顔を見たのがいわゆる人間というものの見始みはじめであろう。この時妙なものだと思った感じが今でも残っている。第一毛をもって装飾されべきはずの顔がつるつるしてまるで薬缶やかんだ。その後ご猫にもだいぶ逢あったがこんな片輪かたわには一度も出会でくわした事がない。<bc>
のみならず顔の真中があまりに突起している。そうしてその穴の中から時々ぷうぷうと煙けむりを吹く。どうも咽むせぽくて実に弱った。これが人間の飲む煙草たばこというものである事はようやくこの頃知った。<but>
<typesound "Type Text">
<dg "Main Choice">
OKAY LETS TRY THIS AGAIN

OKAY??

#Jump Start
<shake><sine>Waaaaaaiiiiiiiiittttt!</sine></shake><but><clear>
This is a very Very VERY dark game. <but><noskip>You might want to QUIT.<but><skip>
<messagescene "Jump" "gotoScene" ["The School"]>

