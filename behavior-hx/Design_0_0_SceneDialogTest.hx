package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.events.Event;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;



class Design_0_0_SceneDialogTest extends SceneScript
{          	
	
public var _TextDisplay:String;
            public var dialog:Dialog = null;

 
 	public function new(dummy:Int, engine:Engine)
	{
		super(engine);
		nameMap.set("Text Display", "_TextDisplay");
_TextDisplay = "";

	}
	
	override public function init()
	{
		            dialog = Dialog.get();
        dialog.loadScene(scene);
        Dialog.get().addDialogBox(new DialogBox(Dialog.getDg("1"), null));
        Dialog.dialogBoxes[Dialog.dialogBoxes.length - 1].beginDialog();
    addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        dialog.drawDialogBoxes();
}
});
    addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        dialog.updateDialogBoxes();
}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}