/**
 * @author Justin Espedal
 */
import com.stencyl.models.Sound;

class SkipScripts extends DialogExtension
{
	//Communicates with TypingScripts

	public var msgSkippable:Bool;
	public var curSkipLevel:Int;
	public var soundTimer:Int;
	public var restoreSpeedTo:Float;
	
	public var typingScript:TypingScripts;
	
	public function new(dg:DialogBox)
	{
		super(dg);
		
		name = "Skip Scripts";
		
		msgSkippable = true;
		curSkipLevel = 0;
		soundTimer = 0;
		restoreSpeedTo = 0;
		
		//skip levels: default, fast skip, zoom skip, instant skip
		
		cmds =
		[
			"skip"=>skip,
			"noskip"=>noskip
		];
		
		addCallback(Dialog.WHEN_CREATED, function():Void
		{
			msgSkippable = getPref("skippableDefault");
			typingScript = cast(dg.getExt("Typing Scripts"), TypingScripts);
		});
		addCallback(Dialog.ALWAYS, function():Void
		{
			if(msgSkippable)
			{
				if(s.isKeyPressed(getPref("instantButton")))
				{
					var snd:Sound = Util.sound(getPref("instantSound"));
					if(snd != null)
						s.playSound(snd);
				}
				if(s.isKeyDown(getPref("instantButton")))
				{
					setTypingScriptSounds(null);
					
					if((curSkipLevel == 1 && dg.msgTypeSpeed != getPref("fastSpeed")) ||
					   (curSkipLevel == 2 && dg.msgTypeSpeed != getPref("zoomSpeed")) ||
					   (curSkipLevel == 3 && dg.msgTypeSpeed != 0) ||
					   (curSkipLevel == 0))
						restoreSpeedTo = dg.msgTypeSpeed;
						
					curSkipLevel = 3;
					
					dg.msgTypeSpeed = 0;
					
					dg.stepTimer = dg.typeDelay = 0;
				}
				else if(s.isKeyDown(getPref("zoomButton")))
				{
					if(getPref("zoomSoundInterval") > 0)
					{
						setTypingScriptSounds(null);
						
						soundTimer += 10;
						if(soundTimer >= getPref("zoomSoundInterval") && !(dg.paused))
						{
							soundTimer = 0;
							var snd:Sound = Util.sound(getPref("zoomSound"));
							if(snd != null)
								s.playSound(snd);
						}
					}
					else 
						setTypingScriptSounds([Util.sound(getPref("zoomSound"))]);
					
					if((curSkipLevel == 1 && dg.msgTypeSpeed != getPref("fastSpeed")) ||
					   (curSkipLevel == 2 && dg.msgTypeSpeed != getPref("zoomSpeed")) ||
					   (curSkipLevel == 3 && dg.msgTypeSpeed != 0) ||
					   (curSkipLevel == 0))
						restoreSpeedTo = dg.msgTypeSpeed;
					
					curSkipLevel = 2;
					
					dg.stepTimer = dg.typeDelay = Std.int(getPref("zoomSpeed") * 1000);
					
					dg.msgTypeSpeed = getPref("zoomSpeed");
				}
				else if(s.isKeyDown(getPref("fastButton")))
				{
					if(getPref("fastSoundInterval") > 0)
					{
						setTypingScriptSounds(null);
						
						soundTimer += 10;
						if(soundTimer >= getPref("fastSoundInterval") && !(dg.paused))
						{
							soundTimer = 0;
							var snd:Sound = Util.sound(getPref("fastSound"));
							if(snd != null)
								s.playSound(snd);
						}
					}
					else
						setTypingScriptSounds([Util.sound(getPref("fastSound"))]);
					
					if((curSkipLevel == 1 && dg.msgTypeSpeed != getPref("fastSpeed")) ||
					   (curSkipLevel == 2 && dg.msgTypeSpeed != getPref("zoomSpeed")) ||
					   (curSkipLevel == 3 && dg.msgTypeSpeed != 0) ||
					   (curSkipLevel == 0))
						restoreSpeedTo = dg.msgTypeSpeed;
					
					curSkipLevel = 1;
					
					dg.msgTypeSpeed = getPref("fastSpeed");
				}
				else if(curSkipLevel != 0)
				{
					soundTimer = 0;
					
					dg.msgTypeSpeed = restoreSpeedTo;
					
					setTypingScriptSounds(typingScript.typeSoundStore);
					
					curSkipLevel = 0;
				}
			}
			else if(curSkipLevel != 0)
			{
				curSkipLevel = 0;
				
				dg.msgTypeSpeed = restoreSpeedTo;
				setTypingScriptSounds(typingScript.typeSoundStore);
			}
		});
	}
	
	public function skip():Void
	{
		msgSkippable = true;
	}
	
	public function noskip():Void
	{
		msgSkippable = false;
		curSkipLevel = 0;
	}

	private function setTypingScriptSounds(sounds:Array<Sound>):Void
	{
		if(TypingScripts != null)
			typingScript.typeSounds = sounds;
	}
}