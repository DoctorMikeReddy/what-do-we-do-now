/**
 * @author Justin Espedal
 */
import com.stencyl.models.Sound;

class TypingScripts extends DialogExtension
{
	public var typeSounds:Array<Sound>;
	public var typeSoundStore:Array<Sound>;
	public var stopTypeSound:Bool;
	
	public function new(dg:DialogBox)
	{
		super(dg);
		
		name = "Typing Scripts";
		
		typeSounds = null;
		typeSoundStore = null;
		stopTypeSound = false;
		
		cmds =
		[
			"font"=>typefont,
			"color"=>typecolor,
			"typespeed"=>typespeed,
			"typesound"=>setTypeSound
		];
		
		addCallback(Dialog.WHEN_CREATED, function():Void
		{
			typeSounds = new Array<Sound>();
			
			if(Std.is(getPref("defaultTypeSound"), String))
			{
				if(getPref("defaultTypeSound") != "")
					typeSounds.push(Util.sound(getPref("defaultTypeSound")));
			}
			else if(Std.is(getPref("defaultTypeSound"), Array))
			{
				for(curSnd in cast(getPref("defaultTypeSound"), Array<Dynamic>))
					typeSounds.push(Util.sound(curSnd));
			}
			typeSoundStore = typeSounds;
		});
		addCallback(Dialog.WHEN_CHAR_TYPED, function():Void
		{
			if(!stopTypeSound && dg.msg[dg.typeIndex] != " ")
			{
				var snd:Sound = getTypeSound();
				if(snd != null)
					s.playSound(snd);
			}
		});
	}
	
	public function typefont(fontName:String):Void
	{
		dg.msgFont = DialogFontLibrary.getFont(fontName);
	}
	
	public function typecolor(fontColor:Int):Void
	{
		dg.msgColor = fontColor;
	}
	
	public function typespeed(speed:Float):Void
	{
		dg.msgTypeSpeed = speed;
	}
	
	public function setTypeSound(sound:Dynamic):Void
	{
		if(sound == "none")
			typeSounds = typeSoundStore = new Array<Sound>();
		else
		{
			typeSounds = new Array<Sound>();
			
			if(Std.is(sound, String))
			{
				typeSounds.push(Util.sound(sound));
			}
			else if(Std.is(sound, Array))
			{
				for(curSnd in cast(sound, Array<Dynamic>))
					typeSounds.push(Util.sound(curSnd));
			}
			typeSoundStore = typeSounds;
		}	
	}
	
	public function getTypeSound():Sound
	{
		if(typeSounds == null || typeSounds.length == 0)
			return null;
		else if(typeSounds.length == 1)
			return typeSounds[0];
		else
			return typeSounds[Std.random(typeSounds.length)];
	}
}