/**
 * @author Justin Espedal
 */
import com.stencyl.models.Sound;

import nme.display.BitmapData;
import nme.geom.Point;

class DialogOptions extends DialogExtension
{
	public var visible:Bool;
	public var options:Array<String>;
	public var targets:Array<String>;
	public var coords:Array<Point>;
	public var curCoord:Point;
	public var usedFont:DialogFont;
	public var selectedIndex:Int;
	public var window:DialogWindow;
	public var baseSelectionImg:BitmapData;
	public var selectionImg:BitmapData;
	public var choiceImg:BitmapData;
	public var upElapsed:Int;
	public var downElapsed:Int;
	public var scrolling:Int;
	public var scrollElapsed:Int;
	public var windowPause:Int;
	public var selectedTarget:String;
	
	public function new(dg:DialogBox)
	{
		super(dg);
		
		name = "Dialog Options";
		
		visible = false;
		options = new Array<String>();
		targets = new Array<String>();
		coords = new Array<Point>();
		curCoord = new Point(0, 0);
		usedFont = null;
		selectedIndex = -1;
		window = null;
		baseSelectionImg = null;
		selectionImg = null;
		choiceImg = null;
		upElapsed = 0;
		downElapsed = 0;
		scrolling = 0;
		scrollElapsed = 0;
		windowPause = 0;
		selectedTarget = "";
		
		cmds =
		[
			"option"=>option,
			"choice"=>option,
		];
		
		addCallback(Dialog.ALWAYS, function():Void
		{
			if(!visible || window == null)
				return;
			
			window.update();
			
			if(windowPause > 0)
			{
				windowPause -= 10;
				return;
			}
			
			if(window.tween != null)
				return;
			
			if(scrolling == 0)
			{
				if(s.isKeyDown("up") || s.isKeyDown("left"))
				{
					downElapsed = 0;
					upElapsed += 10;
					if(upElapsed > getPref("scrollWait"))
					{
						upElapsed = 0;
						scrolling = -1;
					}
				}
				else if(s.isKeyDown("down") || s.isKeyDown("right"))
				{
					upElapsed = 0;
					downElapsed += 10;
					if(downElapsed > getPref("scrollWait"))
					{
						downElapsed = 0;
						scrolling = 1;
					}
				}
				else
				{
					upElapsed = downElapsed = 0;
				}
			}
			else
			{
				if(scrolling == -1 && !(s.isKeyDown("up") || s.isKeyDown("left")))
					scrolling = 0;
				else if(scrolling == 1 && !(s.isKeyDown("down") || s.isKeyDown("right")))
					scrolling = 0;
				else
				{
					scrollElapsed += 10;
					if(scrollElapsed > getPref("scrollDuration"))
					{
						scrollElapsed = 0;
						moveSelection(scrolling);
					}
				}
			}
			
			if(scrolling == 0)
			{
				if(s.isKeyPressed("up") || s.isKeyPressed("left"))
				{
					moveSelection(-1);
				}
				else if(s.isKeyPressed("down") || s.isKeyPressed("right"))
				{
					moveSelection(1);
				}
			}
			if(s.isKeyPressed(getPref("selectChoiceButton")))
			{
				targetSelected();
			}
		});
		addDrawCallback("Draw Dialog Choice", function():Void
		{
			if(!visible || window == null)
				return;

			window.draw(g);
			
			if(window.tween == null)
			{
				G2.drawImage(selectionImg, Std.int(curCoord.x), Std.int(curCoord.y));
				G2.drawImage(choiceImg, Std.int(window.position.x + getPref("typeOffset").x), Std.int(window.position.y + getPref("typeOffset").y));
			}
		});
	}
	
	public function option(args:Array<Array<Dynamic>>):Void
	{
		window = DialogWindowLibrary.getWindow(getPref("window"));
		
		visible = true;
		selectedIndex = 0;

		for(arg in args)
		{
			if(arg.length < 3 || arg[2] == true)
			{
				options.push(arg[0]);
				targets.push(arg[1]);
			}
		}
		
		baseSelectionImg = Util.img(getPref("selectionImage"));
		
		var f:DialogFont = usedFont = DialogFontLibrary.getFont(getPref("windowFont"));

		var w:Int = 0;
		var h:Int = 0;
		var i:Int = 0;
		while(i < options.length)
		{
			if(getPref("choiceLayout") == "Vertical")
			{
				h += f.info.lineHeight + getPref("itemPadding");
				if(f.info.getWidth(options[i]) > w)
					w = f.info.getWidth(options[i]);
			}
			else
			{
				w += f.info.getWidth(options[i]) + getPref("itemPadding");
			}
			++i;
		}
		if(getPref("choiceLayout") == "Horizontal")
		{
			w -= Std.int(getPref("itemPadding"));
			h = f.info.lineHeight;
		}
		else
		{
			h -= Std.int(getPref("itemPadding"));
		}
		
		choiceImg = new BitmapData(w, h, true, 0);
		var x:Int = 0;
		var y:Int = 0;
		for(i in 0...options.length)
		{
			for(j in 0...options[i].length)
			{
				BitmapDataUtil.drawChar(options[i].charAt(j), f, choiceImg, x, y);
				x += f.info.getAdvance(options[i].charAt(j));
			}

			if(getPref("choiceLayout") == "Vertical")
			{
				x = 0;
				y += f.info.lineHeight + getPref("itemPadding");
			}
			else
				x += getPref("itemPadding");
		}

		window.setContentSize(choiceImg.width, choiceImg.height);
		updateSelectionImg();

		dg.paused = true;
		
		windowPause = getPref("inactiveTime");
		
		window.tweenCompleteNotify.push(function():Void
		{
			setCoords();
		});
		window.applyTween(DialogWindowLibrary.getWindowDefinition(getPref("window")).get("create_tween"));
		
		var snd:Sound = Util.sound(getPref("selectionAppearSound"));
		if(snd != null)
			s.playSound(snd);
	}
	
	public function setCoords():Void
	{
		coords = new Array<Point>();
		var f:DialogFont = usedFont;
		
		var x:Int = window.position.x + getPref("typeOffset").x;
		var y:Int = window.position.y + getPref("typeOffset").y;
		for(i in 0...options.length)
		{
			coords.push(new Point(x, y));
			if(getPref("choiceLayout") == "Vertical")
				y += f.info.lineHeight + getPref("itemPadding");
			else
				x += f.info.getWidth(options[i]) + getPref("itemPadding");
		}

		curCoord = new Point(coords[0].x + getPref("selectionOffset").x, coords[0].y + getPref("selectionOffset").y);
	}
	
	public function moveSelection(dir:Int):Void
	{
		if(dir == -1)
		{
			if(selectedIndex > 0)
				selectedIndex -= 1;
			else
				selectedIndex = options.length - 1;
			tweenSelection();
		}
		else if(dir == 1)
		{
			if(selectedIndex < options.length - 1)
				selectedIndex += 1;
			else
				selectedIndex = 0;
			tweenSelection();
		}
		
		var snd:Sound = Util.sound(getPref("selectionChangeSound"));
		if(snd != null)
			s.playSound(snd);
	}

	private function tweenSelection():Void
	{
		curCoord.x = coords[selectedIndex].x + getPref("selectionOffset").x;
		curCoord.y = coords[selectedIndex].y + getPref("selectionOffset").y;
		
		updateSelectionImg();
	}

	private function updateSelectionImg():Void
	{
		var rp:DynamicPoint = getPref("selectionResize");
		var newW:Int = baseSelectionImg.width;
		var newH:Int = baseSelectionImg.height;
		
		var f:DialogFont = usedFont;
		var itemDim:Point = new Point(choiceImg.width, choiceImg.height);
		if(getPref("choiceLayout") == "Vertical")
			itemDim.y = f.info.lineHeight;
		else
			itemDim.x = f.info.getWidth(options[selectedIndex]);
		
		if(rp.xIsPercent())
			newW = itemDim.x * (rp.x / 100) + getPref("selectionResizePadding").x;
		else if(rp.x != -1)
			newW = Std.int(rp.x);
		if(rp.yIsPercent())
			newH = itemDim.y * (rp.y / 100) + getPref("selectionResizePadding").y;
		else if(rp.y != -1)
			newH = Std.int(rp.y);
		
		if(selectionImg != null && newW == selectionImg.width && newH == selectionImg.height)
			return;
		
		selectionImg = BitmapDataUtil.scaleBitmapEdges(newW, newH, baseSelectionImg, getPref("selectionCornerDimension").x, getPref("selectionCornerDimension").y, (getPref("selectionScaleType") == "Stretch"));
	}

	private function targetSelected():Void
	{
		selectedTarget = targets[selectedIndex];
		
		var snd:Sound = Util.sound(getPref("selectionConfirmSound"));
		if(snd != null)
			s.playSound(snd);
		
		window.tweenCompleteNotify.push(function():Void
		{
			visible = false;
			options = new Array<String>();
			targets = new Array<String>();
			coords = new Array<Point>();
			curCoord = new Point(0, 0);
			usedFont = null;
			selectedIndex = -1;
			baseSelectionImg = null;
			selectionImg = null;
			choiceImg = null;
			
			var t:String = selectedTarget;
			selectedTarget = "";

			dg.goToDialog(t);
		});
		window.applyTween(DialogWindowLibrary.getWindowDefinition(getPref("window")).get("destroy_tween"));
	}
}