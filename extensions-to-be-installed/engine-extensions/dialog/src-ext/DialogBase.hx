/**
 * @author Justin Espedal
 */
import com.stencyl.models.Sound;

class DialogBase extends DialogExtension
{
	public var window:DialogWindow;
	public var messageBegan:Bool;
	
	public function new(dg:DialogBox)
	{
		super(dg);
		
		name = "Dialog Base";
		
		window = null;
		messageBegan = false;
		
		cmds =
		[
			"br"=>br,
			"end"=>end,
			"clear"=>clear,
			"close"=>close,
			"dg"=>dgGo
		];
		
		addCallback(Dialog.WHEN_CREATED, function():Void
		{
			dg.paused = true;
			
			window = DialogWindowLibrary.getWindow(getPref("window"));
			window.setContentSize(dg.msgW, dg.msgH);
			window.tweenCompleteNotify.push(function():Void
			{
				dg.paused = false;
			});
			window.applyTween(DialogWindowLibrary.getWindowDefinition(getPref("window")).get("create_tween"));
		});
		addCallback(Dialog.WHEN_MESSAGE_BEGINS, function():Void
		{
			if(!messageBegan)
			{
				messageBegan = true;
				var snd:Sound = Util.sound(getPref("msgStartSound"));
				if(snd != null)
					s.playSound(snd);
			}
			
			s.setGameAttribute(getPref("controlAttribute"), true);
		});
		addCallback(Dialog.WHEN_MESSAGE_ENDS, function():Void
		{
			s.setGameAttribute(getPref("controlAttribute"), false);
			messageBegan = false;
		});
		addCallback(Dialog.ALWAYS, function():Void
		{
			window.update();
		});
		addDrawCallback("Draw Window Frame", function():Void
		{
			window.draw(g);
		});
		addDrawCallback("Draw Message", function():Void
		{
			for(line in dg.lines)
			{
				G2.drawImage(line.img, line.pos.x, line.pos.y);
			}
		});
	}
	
	public function br():Void
	{
		dg.startNextLine();
	}
	
	public function end():Void
	{
		dg.clearMessage();
		
		var snd:Sound = Util.sound(getPref("endSound"));
		if(snd != null)
			s.playSound(snd);
				
		window.tweenCompleteNotify.push(function():Void
		{
			dg.endMessage();
		});
		window.applyTween(DialogWindowLibrary.getWindowDefinition(getPref("window")).get("destroy_tween"));
	}
	
	public function clear():Void
	{
		dg.clearMessage();
		var snd:Sound = Util.sound(getPref("clearSound"));
		if(snd != null)
			s.playSound(snd);
	}
	
	public function close():Void
	{
		dg.closeMessage();
		var snd:Sound = Util.sound(getPref("closeSound"));
		if(snd != null)
			s.playSound(snd);
	}

	public function dgGo(toCall:String):Void
	{
		dg.goToDialog(toCall);
	}
}