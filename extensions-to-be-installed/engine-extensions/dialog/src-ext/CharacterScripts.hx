/**
 * @author Justin Espedal
 */
import com.stencyl.models.Font;
import com.stencyl.Engine;

import nme.display.BitmapData;
	
class CharacterScripts extends DialogExtension
{
	public var nameBitmap:BitmapData;
	public var nameboxWindow:DialogWindow;
	public var nameboxVisible:Bool;
	public var faceBitmap:BitmapData;
	
	public function new(dg:DialogBox)
	{
		super(dg);
		
		name = "Character Scripts";
		
		nameBitmap = null;
		nameboxWindow = null;
		nameboxVisible = false;
		faceBitmap = null;
		
		cmds =
		[
			"showname"=>showname,
			"hidename"=>hidename,
			"face"=>face
		];
		
		addCallback(Dialog.WHEN_CREATED, function():Void
		{
			if(nameboxWindow == null)
				nameboxWindow = DialogWindowLibrary.getWindow(getPref("nameboxWindow"));
			faceBitmap = null;
		});
		addCallback(Dialog.RESTORE_DEFAULTS, function():Void
		{
			faceBitmap = null;
		});
		addCallback(Dialog.ALWAYS, function():Void
		{
			nameboxWindow.update();
		});
		addDrawCallback("Draw Namebox", function():Void
		{
			if(nameboxVisible)
			{
				nameboxWindow.draw(g);
				
				if(nameboxWindow.tween == null)
				{
					var x:Int = getPref("typeXOffset");
					var y:Int = getPref("typeYOffset");
					
					G2.drawImage(nameBitmap, nameboxWindow.position.x + x, nameboxWindow.position.y + y);
				}
			}
		});
		addDrawCallback("Draw Face", function():Void
		{
			if(faceBitmap != null)
			{
				G2.drawImage(faceBitmap, Std.parseFloat(getPref("faceX")), Std.parseFloat(getPref("faceY")));
			}
		});
	}
	
	public function showname(nameToDraw:String):Void
	{
		nameboxVisible = true;
		
		var f:DialogFont = DialogFontLibrary.getFont(getPref("nameboxFont"));
		nameBitmap = new BitmapData(f.info.getWidth(nameToDraw), f.info.lineHeight, true, 0);
		nameboxWindow.setContentSize(nameBitmap.width, nameBitmap.height);
		var x:Int = 0;
		for(i in 0...nameToDraw.length)
		{
			BitmapDataUtil.drawChar(nameToDraw.charAt(i), f, nameBitmap, x, 0);
			x += f.info.getAdvance(nameToDraw.charAt(i));
		}
		nameboxWindow.applyTween(DialogWindowLibrary.getWindowDefinition(getPref("nameboxWindow")).get("create_tween"));
	}
	
	public function hidename():Void
	{
		nameboxWindow.tweenCompleteNotify.push(function():Void
		{
			nameboxVisible = false;
		});
		nameboxWindow.applyTween(DialogWindowLibrary.getWindowDefinition(getPref("nameboxWindow")).get("destroy_tween"));
	}
	
	public function face(facename:String):Void
	{
		if(facename == "none")
		{
			faceBitmap = null;
			dg.msgX = dg.style.getPref("Dialog Base", "msgX");
			dg.msgY = dg.style.getPref("Dialog Base", "msgY");
			dg.msgW = dg.style.getPref("Dialog Base", "msgW");
			dg.msgH = dg.style.getPref("Dialog Base", "msgH");
		}
		else
		{
			faceBitmap = Util.img(getPref("faceImagePrefix") + facename);
			dg.msgX = getPref("faceMessageX");
			dg.msgY = getPref("faceMessageY");
			dg.msgW = getPref("faceMessageWidth");
			dg.msgH = getPref("faceMessageHeight");
		}
	}
}