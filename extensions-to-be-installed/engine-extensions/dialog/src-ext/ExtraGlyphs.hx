/**
 * @author Justin Espedal
 */
import nme.display.BitmapData;
import nme.geom.Point;

class ExtraGlyphs extends DialogExtension
{
	public function new(dg:DialogBox)
	{
		super(dg);
		
		name = "Extra Glyphs";
		
		cmds =
		[
			"glyph"=>glyph
		];
	}

	public function glyph(glyphName:String):Void
	{
		var img:BitmapData = Util.img(glyphName);
		
		if(dg.drawX + img.width > dg.msgW)
			dg.startNextLine();
		if(dg.drawHandler != null)
		{
			var charID:Int = dg.drawHandler.addImg(img, dg.msgX + dg.drawX, Std.int(dg.curLine.pos.y) + (dg.curLine.aboveBase - img.height));
			dg.curLine.drawHandledChars.push(new DrawHandledImage(dg.drawHandler, charID));
		}		
		else
			dg.curLine.img.copyPixels(img, img.rect, new Point(dg.drawX, dg.curLine.aboveBase - img.height));
		dg.drawX += img.width + getPref("padding");
		dg.typeDelay = Std.int(dg.msgTypeSpeed * 1000);
		dg.runCallbacks(Dialog.WHEN_CHAR_TYPED);
	}
}