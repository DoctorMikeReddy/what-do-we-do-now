/**
 * @author Justin Espedal
 */
import DialogBase;
import CharacterScripts;
import FlowScripts;
import MessagingScripts;
import SkipScripts;
import SoundScripts;
import TypingScripts;
import ExtraGlyphs;
import TextEffects;
import DialogOptions;
import Logic;

class DialogStyle
{
	public var name:String;
	
	public var extensionClasses:Array<Class<Dynamic>>;
	public var extensions:Array<DialogExtension>;
	public var extensionMap:Map<String, DialogExtension>;
	
	public var preferences:Map<String, Map<String, Dynamic>>; //extName, prefName -> Dynamic
	public var layers:Array<String>;
	
	public var callbacks:Map<Int, Array<Void->Void>>; //callback const, index -> function():Void
	public var graphicsCallbacks:Map<String, Void->Void>; //layer name -> function():Void
	public var cmds:Map<String, Dynamic>; //cmdName -> function(Dynamic):Dynamic

	public static function fromFile(name:String):DialogStyle
	{
		var lines:Array<String> = Util.getFileLines(name + ".style");
		
		var n_pref:Map<String, Map<String, Dynamic>> = new Map<String, Map<String, Dynamic>>();
		var n_draw:Array<String> = new Array<String>();
		var n_ext:Array<Class<Dynamic>> = new Array<Class<Dynamic>>();
		
		var indices:Array<Int> = Util.getTitleIndices(lines, ">");
		var title:String;
		
		for(i in indices)
		{
			title = lines[i].substr(1);
			
			if(title == "Draw Order")
				n_draw = Util.arrayFrom(lines, i + 1);
			else if(title == "Extensions")
			{
				var c:Class<Dynamic> = null;
				for(curClassName in Util.arrayFrom(lines, i + 1))
				{
					c = Type.resolveClass(curClassName);

					if(c != null)
						n_ext.push(c);
				}
			}
			else
				n_pref.set(title, Util.keyValuesFrom(lines, i + 1));
		}
		
		return new DialogStyle(name, n_ext, n_pref, n_draw);
	}
	
	public function new(name:String, extensionClasses:Array<Class<Dynamic>>, preferences:Map<String, Map<String, Dynamic>>, layers:Array<String>)
	{
		this.name = name;
		this.extensionClasses = extensionClasses;
		this.preferences = preferences;
		this.layers = layers;
	}
	
	public function tieExtensionsToDialogBox(dg:DialogBox):Void
	{
		extensions = new Array<DialogExtension>();
		extensionMap = new Map<String, DialogExtension>();
		
		var curExt:DialogExtension;
		
		for(curClass in extensionClasses)
		{
			curExt = Type.createInstance(curClass, [dg]);
			this.extensions.push(curExt);
			this.extensionMap.set(curExt.name, curExt);
			
			curExt.prefs = preferences.get(curExt.name);
		}
		
		cmds = new Map<String, Dynamic>();
		for(curExtension in this.extensions)
		{
			for(curCmdName in curExtension.cmds.keys())
			{
				cmds.set(curCmdName, curExtension.cmds.get(curCmdName));
			}
		}
		
		callbacks = new Map<Int, Array<Void->Void>>();
		for(curConst in Dialog.callbackConstants)
		{
			inheritCallbacks(curConst);
		}
		inheritGraphicsCallbacks();
	}
	
	public function getPref(extName:String, prefName:String):Dynamic
	{
		return preferences.get(extName).get(prefName);
	}
	
	private function inheritCallbacks(constID:Int):Void
	{
		if(constID == Dialog.WHEN_DRAWING)
			return;

		if(!callbacks.exists(constID))
			callbacks.set(constID, new Array<Void->Void>());
		
		for(curExtension in extensions)
		{
			if(curExtension.callbacks.exists(constID))
			{
				for(curFunction in curExtension.callbacks.get(constID))
				{
					callbacks.get(constID).push(curFunction);
				}
			}
		}
	}

	private function inheritGraphicsCallbacks():Void
	{
		graphicsCallbacks = new Map<String, Void->Void>();

		for(curExtension in extensions)
		{
			if(curExtension.graphicsCallbacks != null)
			{
				for(key in curExtension.graphicsCallbacks.keys())
				{
					graphicsCallbacks.set(key, curExtension.graphicsCallbacks.get(key));
				}
			}
		}
	}
}