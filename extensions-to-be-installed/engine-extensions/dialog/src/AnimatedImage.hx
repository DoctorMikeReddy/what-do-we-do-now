/**
 * @author Justin Espedal
 */

import nme.display.BitmapData;
import nme.geom.Point;
import nme.geom.Rectangle;

class AnimatedImage
{
	public var info:AnimationInfo;
	public var repeats:Bool;
	public var elapsed:Int;
	public var curFrame:Int;
	public var numFrames:Int;
	public var durations:Array<Int>;
	public var sheet:BitmapData;

	public var done:Bool;

	public var curFrameImg:BitmapData;
	
	public function new(name:String)
	{
		info = AnimationLibrary.getAnimInfo(name);
		repeats = info.repeats;
		elapsed = 0;
		curFrame = 0;
		numFrames = info.frameCount;
		durations = info.durations;
		sheet = info.sheet;

		curFrameImg = copyFrame(0);
	}

	public function start():Void
	{
		Dialog.get().addAnimation(this);
	}

	public function end():Void
	{
		Dialog.get().removeAnimation(this);
	}

	public function draw(x:Int, y:Int):Void
	{
		G2.drawImage(curFrameImg, x, y);
	}

	public function update():Void
	{
		if(done)
			return;

		elapsed += 10;
		if(elapsed >= durations[curFrame])
		{
			++curFrame;
			elapsed = 0;
			if(curFrame >= numFrames)
			{
				if(!repeats)
				{
					--curFrame;
					done = true;
				}
				else
					curFrame = 0;
			}

			curFrameImg = copyFrame(curFrame);
		}
	}

	private static var zeroPoint:Point = new Point(0, 0);

	public function copyFrame(frame:Int):BitmapData
	{
		var img:BitmapData = new BitmapData(info.width, info.height, true, 0);
		img.copyPixels(sheet, new Rectangle(info.width * frame, 0, info.width, info.height), zeroPoint, null, null, true);
		return img;
	}
}