/**
 * @author Justin Espedal
 */
import com.stencyl.graphics.G;
import com.stencyl.Engine;

import nme.display.BitmapData;
import nme.geom.ColorTransform;
import nme.geom.Point;

class DialogWindow
{
	public static inline var SCALE_NONE = 0;
	public static inline var SCALE_FIT_CONTENTS = 1;
	public static inline var SCALE_ABSOLUTE = 2;
	
	public static inline var SCALE_STRETCH = 0;
	public static inline var SCALE_TILE = 1;
	
	public static inline var SCALE_WHOLE = 0;
	public static inline var SCALE_INSIDE = 1;
	
	public var positionRef:DynamicPoint;
	public var sizeRef:Point;
	public var imageName:String;
	
	public var scaleWidth:Int;
	public var scaleHeight:Int;
	public var scaleType:Int;
	public var scalePart:Int;
	public var cornerDimension:Point;
	public var contentPadding:Point;
	
	public var position:Point;
	public var absoluteSize:Point;
	public var image:BitmapData;
	public var tween:WindowTween;
	
	public var tweenCompleteNotify:Array<Void->Void>;
	
	public function new(positionRef:DynamicPoint, sizeRef:Point, imageName:String, scaleWidth:Int, scaleHeight:Int, scaleType:Int, scalePart:Int, cornerDimension:Point, contentPadding:Point)
	{
		this.positionRef = positionRef;
		this.sizeRef = sizeRef;
		this.imageName = imageName;
		this.scaleWidth = scaleWidth;
		this.scaleHeight = scaleHeight;
		this.scaleType = scaleType;
		this.scalePart = scalePart;
		this.cornerDimension = cornerDimension;
		this.contentPadding = contentPadding;
		
		image = Util.img(imageName);
		absoluteSize = new Point(sizeRef.x, sizeRef.y);
		if(scaleWidth == SCALE_FIT_CONTENTS)
			absoluteSize.x = image.width + contentPadding.x;
		if(scaleHeight == SCALE_FIT_CONTENTS)
			absoluteSize.y = image.height + contentPadding.y;
		
		position = new Point(0, 0);
		reposition();
		
		tween = null;
		tweenCompleteNotify = new Array<Void->Void>();
		
		updateImg();
	}
	
	public function setContentSize(w:Int, h:Int)
	{
		if(scaleWidth == SCALE_FIT_CONTENTS)
			absoluteSize.x = w + contentPadding.x;
		if(scaleHeight == SCALE_FIT_CONTENTS)
			absoluteSize.y = h + contentPadding.y;
		
		reposition();
		updateImg();
	}
	
	public function reposition():Void
	{
		if(positionRef.xIsPercent())
			position.x = (Engine.screenWidth - absoluteSize.x) * (positionRef.x / 100);
		else if(positionRef.xIsCenter())
			position.x = positionRef.x - absoluteSize.x / 2;
		else
			position.x = positionRef.x;
		if(positionRef.yIsPercent())
			position.y = (Engine.screenHeight - absoluteSize.y) * (positionRef.y / 100);
		else if(positionRef.yIsCenter())
			position.y = positionRef.y - absoluteSize.y / 2;
		else
			position.y = positionRef.y;
	}
	
	public function updateImg():Void
	{
		var newW:Int = -1;
		var newH:Int = -1;
		
		if(image.width != absoluteSize.x)
			newW = Std.int(absoluteSize.x);
		if(image.height != absoluteSize.y)
			newH = Std.int(absoluteSize.y);
		
		if(newW == -1 && newH == -1)
			return;
		
		if(scalePart == SCALE_WHOLE)
		{
			image = Util.img(imageName);
			if(scaleType == SCALE_STRETCH)
			{
				image = BitmapDataUtil.scaleBitmap(image, absoluteSize.x / image.width, absoluteSize.y / image.height);
			}
			else
			{
				image = BitmapDataUtil.tileBitmap(image, absoluteSize.x / image.width, absoluteSize.y / image.height);
			}
		}
		else if(scalePart == SCALE_INSIDE)
			image = BitmapDataUtil.scaleBitmapEdges(Std.int(absoluteSize.x), Std.int(absoluteSize.y), Util.img(imageName), Std.int(cornerDimension.x), Std.int(cornerDimension.y), (scaleType == SCALE_STRETCH));
	}
	
	public function applyTween(tweenName:String):Void
	{
		this.tween = new WindowTween(image, position, tweenName);
	}
	
	public function update():Void
	{
		if(tween == null)
			return;
			
		tween.update(10);
		var progress:Float = tween.elapsed / tween.duration;
		
		var newImg:BitmapData = BitmapDataUtil.scaleBitmap(tween.srcImg, tween.scale.get(progress).x, tween.scale.get(progress).y);
		var ct:ColorTransform = new ColorTransform();
		ct.alphaMultiplier = tween.opacity.get(progress);
		newImg.colorTransform(newImg.rect, ct);
		
		image = newImg;
		position = tween.pos1.get(progress).add(tween.pos2.get(progress));
		
		if(tween.elapsed >= tween.duration)
		{
			tween = null;
			for(f in tweenCompleteNotify)
				f();
			
			tweenCompleteNotify = new Array<Void->Void>();
		}
	}
	
	public function draw(g:G):Void
	{
		G2.drawImage(image, position.x, position.y);
	}
}