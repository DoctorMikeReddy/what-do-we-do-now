/**
 * @author Justin Espedal
 */
import com.stencyl.Engine;

import nme.display.BitmapData;
import nme.geom.Point;

class WindowTween
{
	public var elapsed:Int;
	public var duration:Int;
	
	public var pos1:PointTween; //pos as if image isn't scaled
	public var pos2:PointTween; //corrects for image scale according to scaleAnchor
	public var scale:PointTween;
	public var opacity:FloatTween;
	
	public var srcImg:BitmapData;
	
	public function new(img:BitmapData, p:Point, tweenName:String)
	{
		var prefs:Map<String, Dynamic> = TweenLibrary.getTween(tweenName);
		
		elapsed = 0;
		duration = prefs.get("duration");
		
		var dp1:DynamicPoint;
		var dp2:DynamicPoint;

		if(prefs.exists("position"))
		{
			if(Std.is(prefs.get("position"), Array))
			{
				dp1 = prefs.get("position")[0].clone();
				dp2 = prefs.get("position")[1].clone();
			}
			else
			{
				dp1 = prefs.get("position").clone();
				dp2 = dp1.clone();
			}
		}
		else
		{
			dp1 = new DynamicPoint(p.x, p.y, "", "");
			dp2 = dp1.clone();
		}
		
		if(dp1.xIsPercent())
			dp1.x = getProgressOnSlide(0, Engine.screenWidth - img.width, dp1.x / 100);
		if(dp1.yIsPercent())
			dp1.y = getProgressOnSlide(0, Engine.screenHeight - img.height, dp1.y / 100);
		if(dp2.xIsPercent())
			dp2.x = getProgressOnSlide(0, Engine.screenWidth - img.width, dp2.x / 100);
		if(dp2.yIsPercent())
			dp2.y = getProgressOnSlide(0, Engine.screenHeight - img.height, dp2.y / 100);
		
		pos1 = new PointTween(dp1, dp2);
		
		if(prefs.exists("scale"))
		{
			if(Std.is(prefs.get("scale"), Array))
			{
				dp1 = prefs.get("scale")[0].clone();
				dp2 = prefs.get("scale")[1].clone();
			}
			else
			{
				dp1 = prefs.get("scale").clone();
				dp2 = dp1.clone();
			}
		}
		else
		{
			dp1 = new DynamicPoint(1, 1, "", "");
			dp2 = dp1.clone();
		}
		
		scale = new PointTween(dp1, dp2);
		
		var f1:Float;
		var f2:Float;
		
		if(prefs.exists("opacity"))
		{
			if(Std.is(prefs.get("opacity"), Array))
			{
				f1 = prefs.get("opacity")[0];
				f2 = prefs.get("opacity")[1];
			}
			else
				f1 = f2 = prefs.get("opacity");
		}
		else
			f1 = f2 = 1;
		
		opacity = new FloatTween(f1, f2);
		
		if(prefs.exists("positionScaleAnchor"))
		{
			if(Std.is(prefs.get("positionScaleAnchor"), Array))
			{
				dp1 = prefs.get("positionScaleAnchor")[0].clone();
				dp2 = prefs.get("positionScaleAnchor")[1].clone();
			}
			else
			{
				dp1 = prefs.get("positionScaleAnchor").clone();
				dp2 = dp1.clone();
			}
		}
		else
		{
			dp1 = new DynamicPoint(50, 50, "%", "%");
			dp2 = dp1.clone();
		}
		
		if(dp1.xIsPercent())
			dp1.x = getProgressOnSlide(0, img.width - scale.xStart * img.width, dp1.x / 100);
		if(dp1.yIsPercent())
			dp1.y = getProgressOnSlide(0, img.height - scale.yStart * img.height, dp1.y / 100);
		if(dp2.xIsPercent())
			dp2.x = getProgressOnSlide(0, img.width - scale.xEnd * img.width, dp2.x / 100);
		if(dp2.yIsPercent())
			dp2.y = getProgressOnSlide(0, img.height - scale.yEnd * img.height, dp2.y / 100);
		
		pos2 = new PointTween(dp1, dp2);
		
		srcImg = img;
	}
	
	private function getProgressOnSlide(begin:Float, end:Float, ratio:Float):Float
	{
		return begin + (end - begin) * ratio;
	}
	
	public function update(elapsed:Int):Void
	{
		this.elapsed += elapsed;
		if(elapsed > duration)
			elapsed = duration;
	}
}

private class PointTween
{
	public var xStart:Float;
	public var xEnd:Float;
	public var yStart:Float;
	public var yEnd:Float;
	
	public function new(start:Point, end:Point)
	{
		xStart = start.x;
		xEnd = end.x;
		yStart = start.y;
		yEnd = end.y;
	}
	
	public function get(progress:Float):Point
	{
		return new Point(xStart + (xEnd - xStart) * progress, yStart + (yEnd - yStart) * progress);
	}
	
	public function toString():String
	{
		return xStart + ", " + yStart + "   " + xEnd + ", " + yEnd;
	}
}

private class FloatTween
{
	public var start:Float;
	public var end:Float;
	
	public function new(start:Float, end:Float)
	{
		this.start = start;
		this.end = end;
	}
	
	public function get(progress:Float):Float
	{
		return start + (end - start) * progress;
	}
}