/**
 * @author Justin Espedal
 */
import com.stencyl.Engine;

import nme.geom.Point;

class DialogWindowLibrary
{
	private static var windowDefinitions:Map<String, Map<String, Dynamic>>;
	
	private function new()
	{
		
	}
	
	public static function getWindowDefinition(name:String):Map<String, Dynamic>
	{
		if(windowDefinitions == null)
		{
			windowDefinitions = Util.readPrefsFile("windows");
		}
		
		return windowDefinitions.get(name);
	}
	
	public static function getWindow(name:String):DialogWindow
	{
		if(windowDefinitions == null)
		{
			windowDefinitions = Util.readPrefsFile("windows");
		}
		
		var w:Map<String, Dynamic> = windowDefinitions.get(name);
		
		var scaleWidth:Int;
		var scaleHeight:Int;
		var scaleType:Int = 0;
		var scalePart:Int = 0;
		var sizeRef:Point = new Point(0, 0);
		
		var s:String;
		
		if(!w.exists("scale.width"))
		{
			scaleWidth = DialogWindow.SCALE_NONE;
			sizeRef.x = Util.img(w.get("image")).width;
		}
		else
		{
			s = w.get("scale.width");
			if(s.indexOf("%") != -1)
			{
				scaleWidth = DialogWindow.SCALE_ABSOLUTE;
				sizeRef.x = Std.parseFloat(s.substr(0, s.length - 1)) / 100 * Engine.screenWidth;
			}
			else if(s == "default")
			{
				scaleWidth = DialogWindow.SCALE_NONE;
				sizeRef.x = Util.img(w.get("image")).width;
			}
			else if(s == "fit contents")
			{
				scaleWidth = DialogWindow.SCALE_FIT_CONTENTS;
				sizeRef.x = -1;
			}
			else
			{
				scaleWidth = DialogWindow.SCALE_ABSOLUTE;
				sizeRef.x = Std.parseFloat(s);
			}
		}
		
		if(!w.exists("scale.height"))
		{
			scaleHeight = DialogWindow.SCALE_NONE;
			sizeRef.y = Util.img(w.get("image")).height;
		}
		else
		{
			s = w.get("scale.height");
			if(s.indexOf("%") != -1)
			{
				scaleHeight = DialogWindow.SCALE_ABSOLUTE;
				sizeRef.y = Std.parseFloat(s.substr(0, s.length - 1)) / 100 * Engine.screenHeight;
			}
			else if(s == "default")
			{
				scaleHeight = DialogWindow.SCALE_NONE;
				sizeRef.y = Util.img(w.get("image")).height;
			}
			else if(s == "fit contents")
			{
				scaleHeight = DialogWindow.SCALE_FIT_CONTENTS;
				sizeRef.y = -1;
			}
			else
			{
				scaleHeight = DialogWindow.SCALE_ABSOLUTE;
				sizeRef.y = Std.parseFloat(s);
			}
		}
		
		if(w.exists("scale.type"))
		{
			s = w.get("scale.type");
			if(s == "stretch")
				scaleType = DialogWindow.SCALE_STRETCH;
			else if(s == "tile")
				scaleType = DialogWindow.SCALE_TILE;
		}
		
		if(w.exists("scale.part"))
		{
			s = w.get("scale.part");
			if(s == "whole")
				scalePart = DialogWindow.SCALE_WHOLE;
			else if(s == "inside")
				scalePart = DialogWindow.SCALE_INSIDE;
		}
		
		var positionRef:DynamicPoint = w.get("position");
		var cornerDimension:Point = cast(w.get("scale.cornerDimension"), Point);
		var contentPadding:Point = cast(w.get("scale.contentPadding"), Point);
		
		if(positionRef != null)
			positionRef = positionRef.clone();
		if(cornerDimension != null)
			cornerDimension = cornerDimension.clone();
		if(contentPadding != null)
			contentPadding = contentPadding.clone();
		
		return new DialogWindow(positionRef, sizeRef, w.get("image"), scaleWidth, scaleHeight, scaleType, scalePart, cornerDimension, contentPadding);
	}
}