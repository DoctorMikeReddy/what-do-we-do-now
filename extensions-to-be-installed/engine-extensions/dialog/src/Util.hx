/**
 * @author Justin Espedal
 */
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Sprite;
import com.stencyl.models.Font;
import com.stencyl.models.Resource;
import com.stencyl.models.Sound;
import com.stencyl.Data;

import nme.display.BitmapData;

class Util
{
	public static var r_map:Map<String, Resource> = null;
	
	public function new()
	{
	}

	public static function r(name:String):Resource
	{
		if(r_map == null)
			initR();
		
		return r_map.get(name);
	}

	public static function actorType(name:String):ActorType
	{
		return cast(r(name), ActorType);
	}
	
	public static function sprite(name:String):Sprite
	{
		return cast(r("S_" + name), Sprite);
	}

	public static function font(name:String):Font
	{
		return cast(r(name), Font);
	}
	
	public static function sound(name:String):Sound
	{
		return cast(r(name), Sound);
	}

	public static function img(name:String):BitmapData
	{
		return nme.Assets.getBitmapData("assets/dialog/images/"+ name +".png");
	}
	
	public static function text(name:String):String
	{
		return nme.Assets.getText("assets/dialog/"+ name);
	}

	public static function fontImg(name:String):BitmapData
	{
		return nme.Assets.getBitmapData("assets/dialog/fonts/"+ name +".png");
	}
	
	public static function fontInfo(name:String):String
	{
		return nme.Assets.getText("assets/dialog/fonts/"+ name + ".fnt");
	}

	public static function animInfo(name:String):String
	{
		return nme.Assets.getText("assets/dialog/anims/"+ name + ".anim");
	}

	private static function initR():Void
	{
		r_map = new Map<String, Resource>();
		for(curR in Data.get().resources)
		{
			if(curR == null) continue;
			if(Std.is(curR, Sprite))
				r_map.set("S_"+curR.name, curR);
			else
				r_map.set(curR.name, curR);
		}
	}
	
	public static function readPrefsFile(filename:String):Map<String, Map<String, Dynamic>>
	{
		var prefLines:Array<String> = Util.getFileLines(filename + ".txt");
		
		var n_pref:Map<String, Map<String, Dynamic>> = new Map<String, Map<String, Dynamic>>();
		var indices:Array<Int> = getTitleIndices(prefLines, ">");
		var title:String;
		
		for(i in indices)
		{
			title = prefLines[i].substr(1);
			n_pref.set(title, keyValuesFrom(prefLines, i + 1));
		}
		
		return n_pref;
	}
	
	public static function keyValuesFrom(s_split:Array<String>, s_index:Int):Map<String, Dynamic>
	{
		var n_pref:Map<String, Dynamic> = new Map<String, Dynamic>();
		var curLine:String;
		var pair:Array<String>;
		
		while(s_index < s_split.length)
		{
			curLine = s_split[s_index];
			
			if(curLine.length != 0 && curLine.charAt(0) != "#")
			{
				if(curLine.charAt(0) == ">" || curLine.charAt(0) == "<")
					break;

				pair = curLine.split("=");
				
				n_pref.set(pair[0], valueOfString(pair[1]));
			}
			
			++s_index;
		}
		
		return n_pref;
	}
	
	public static function arrayFrom(s_split:Array<String>, s_index:Int):Array<String>
	{
		var a:Array<String> = new Array<String>();
		var curLine:String;
		
		while(s_index < s_split.length)
		{
			curLine = s_split[s_index];
			
			if(curLine.length != 0 && curLine.charAt(0) != "#")
			{
				if(curLine.charAt(0) == ">")
					break;

				a.push(curLine);
			}
			
			++s_index;
		}
		
		return a;
	}
	
	public static function getTitleIndices(s_split:Array<String>, titleMarker:String):Array<Int>
	{
		var indices:Array<Int> = new Array<Int>();
		
		for(i in 0...s_split.length)
		{
			if(s_split[i].charAt(0) == titleMarker)
				indices.push(i);
		}
		
		return indices;
	}
	
	public static function valueOfString(s:String):Dynamic
	{
		s = trim(s);
		
		//List
		if(s.indexOf(",") != -1)
		{
			var toReturn:Array<Dynamic> = new Array<Dynamic>();
			for(curItem in s.split(","))
			{
				toReturn.push(valueOfString(curItem));
			}
			return toReturn;
		}
		
		//Hexadicimal Int
		if(s.substring(0, 2) == "0x")
		{
			return Std.parseInt(s);
		}
		
		if(s.substring(0, 1) == "#")
		{
			return Std.parseInt("0x" + s.substr(1));
		}
		
		//Float or Int
		if(!Math.isNaN(Std.parseFloat(s)))
			return Std.parseFloat(s);
		
		//Dynamic Point
		if(s.charAt(0) == "(" && s.charAt(s.length - 1) == ")")
		{
			var a:Array<String> = trim(s.substring(1, s.length - 1)).split(" ");
			
			var lastX:Int = (a[0].charCodeAt(a[0].length - 1));
			var lastY:Int = (a[1].charCodeAt(a[1].length - 1));
			var xHasChar:Bool = (lastX < 48 || lastX > 57);
			var yHasChar:Bool = (lastY < 48 || lastY > 57);
			
			if(xHasChar)
				a[0] = a[0].substr(0, a[0].length - 1);
			if(yHasChar)
				a[1] = a[1].substr(0, a[1].length - 1);
			
			return new DynamicPoint(Std.parseFloat(a[0]), Std.parseFloat(a[1]), xHasChar ? String.fromCharCode(lastX) : "", yHasChar ? String.fromCharCode(lastY) : "");
		}
		
		//Bool
		if(s == "true")
			return true;
		
		if(s == "false")
			return false;
		
		//Quoted String
		if(s.charAt(0) == "\"" && s.charAt(s.length - 1) == "\"")
		{
			return s.substring(1, s.length - 1);
		}

		return s;
	}
	
	public static function valueOfString2(s:String):Dynamic
	{
		//Hexadicimal Int
		if(s.substring(0, 2) == "0x")
		{
			return Std.parseInt(s);
		}
		
		if(s.substring(0, 1) == "#")
		{
			return Std.parseInt("0x" + s.substr(1));
		}
		
		//Float or Int
		if(!Math.isNaN(Std.parseFloat(s)))
			return Std.parseFloat(s);
		
		//Bool
		if(s == "true")
			return true;
		
		if(s == "false")
			return false;
		
		return s;
	}
	
	public static function trim(s:String):String
	{
		if(s.charAt(0) != " " && s.charAt(s.length - 1) != " ")
			return s;
		
		var a:Array<String> = s.split("");
		
		while(a[0] == " ")
			a.shift();
		while(a[a.length - 1] == " ")
			a.pop();
		
		return a.join("");
	}
	
	public static var newlinePattern:EReg = ~/[\r\n]+/g;
	
	public static function getLines(s:String):Array<String>
	{
		return newlinePattern.split(s);
	}
	
	public static function getFileLines(filename:String):Array<String>
	{
		return newlinePattern.split(nme.Assets.getText("assets/dialog/"+ filename));
	}
}