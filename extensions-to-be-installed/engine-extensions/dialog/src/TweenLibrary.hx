/**
 * @author Justin Espedal
 */
class TweenLibrary
{
	private static var tweens:Map<String, Map<String, Dynamic>>;
	
	private function new()
	{
		
	}
	
	public static function getTween(name:String):Map<String, Dynamic>
	{
		if(tweens == null)
		{
			tweens = Util.readPrefsFile("tweens");
		}
		
		return tweens.get(name);
	}
}