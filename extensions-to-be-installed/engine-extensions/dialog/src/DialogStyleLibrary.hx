/**
 * @author Justin Espedal
 */
class DialogStyleLibrary
{
	private static var styles:Map<String, DialogStyle>;
	
	private function new()
	{
		
	}
	
	public static function getStyle(name:String):DialogStyle
	{
		if(styles == null)
		{
			styles = new Map<String, DialogStyle>();
			var defaultStyle:DialogStyle = DialogStyle.fromFile("Default Style");
			styles.set("Default Style", defaultStyle);
			styles.set("base", defaultStyle);
		}
		
		if(!styles.exists(name))
			styles.set(name, DialogStyle.fromFile(name));
		
		return styles.get(name);
	}
}