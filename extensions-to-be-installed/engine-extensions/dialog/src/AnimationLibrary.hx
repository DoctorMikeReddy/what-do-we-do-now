/**
 * @author Justin Espedal
 */
class AnimationLibrary
{
	private static var animInfo:Map<String, AnimationInfo>;
	
	private function new()
	{
		
	}
	
	public static function getAnimInfo(name:String):AnimationInfo
	{
		if(animInfo == null)
			animInfo = new Map<String, AnimationInfo>();
		
		if(!animInfo.exists(name))
			animInfo.set(name, new AnimationInfo(name));
		
		return animInfo.get(name);
	}
}