/**
 * @author Justin Espedal
 */
import nme.geom.Point;

class DynamicPoint extends Point
{
	public var xChar:String;
	public var yChar:String;
	
	public function new(x:Float, y:Float, xChar:String, yChar:String)
	{
		super(x, y);
		
		this.xChar = xChar;
		this.yChar = yChar;
	}
	
	public inline function xIsPercent()
	{
		return xChar == "%";
	}
	
	public inline function yIsPercent()
	{
		return yChar == "%";
	}
	
	public inline function xIsCenter()
	{
		return xChar == "c";
	}
	
	public inline function yIsCenter()
	{
		return yChar == "c";
	}
	
	override public function clone():DynamicPoint
	{
		return new DynamicPoint(x, y, xChar, yChar);
	}
}